# **Configurar el Netplan**

1. cd /etc/netplan
2. sudo nano 00-installer-config.yaml
3. CONFIGURACIÓ DE L'ARXIU: 

```
enp0s3:
 addresses: [IP Ordenador/Màscara de red]
 gateway4: IP router
 nameservers:
  addresses: [IP]
  search: [nom del servei]
```
