
# **Servidor DHCP**

1. sudo apt get install isc-dhcp-server -y
2. cd /etc/default
3. sudo cp isc-dhcp-server isc-dhcp-server.old
4. sudo nano isc-dhcp-server
5. CONFIGURACIÓ DE L'ARXIU:

```
INTERFACES="interficie que hi hagi (enp0s3)"
```

6. cd /etc/dhcp
7. sudo cp dhcp.conf dhcp.conf.old
8. sudo nano dhcp.conf
9. CONFIGURACIÓ DE L'ARXIU:

```
# A slight ...
subnet **IP de la xarxa** netmask **Màscara** {
    range **IP començament rang** **IP finalment rang**;
    option domain-name-servers *IP DNS*;
#
    option routers *IP router*;
#
#
#
}
```
10. sudo service isc-dhcp-server restart
